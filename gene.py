#using biopython to determine the species in a DNA sequence

from Bio.Blast import NCBIWWW

#opens the fasta file and read sequence into a variable seq_string
seq_string = open('myseq.fa').read()

#lastn searches for nucleotides against nucleotides in a the nt database 
r_handle = NCBIWWW.qblast("blastn","nt",seq_string)


from Bio.Blast import NCBIXML
#uses the query results in a variable blast_record and reads it in xml format
blast_record = NCBIXML.read(r_handle)

#Uses the results of alignments to determine the alignment,sequence,query,length,sbjct,match and prints them to the console
len(blast_record.alignments)
E_VALUE_THRESH = 0.01#expectation or probability
#loop through the alignments and return the ones with a good e-value
for alignment in blast_record.alignments:
    #hsps -high scoring pairs
    for hsp in alignment.hsps:
        if hsp.expect < E_VALUE_THRESH:
           print('****Alignment****')
           print('Sequence:',alignment.title)
           print('Length:',alignment.length)
           print('e value:',hsp.expect)
           print(hsp.query)
           print(hsp.match)
           print(hsp.sbjct)


               
